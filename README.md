A cli interactive metronome.

Interactive commands:

* `+`, `-` Increase/decrease the tempo.
* `<N>+`, `<N>-` where `<N>` is a number: increase/decrease the tempo by the given step, and save this step
  for further modifications.
* `<N><Return>` Set the tempo to the given number.
* `q` Quit.
