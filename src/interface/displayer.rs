use std::io::{self, prelude::*};
use std::sync::{Arc, Mutex};

use colorful::Color;
use colorful::Colorful;

use crate::core::{Player, TEMPO_PRECISION};
use super::Keyboard;

pub struct Displayer {
    keyboard: Arc<Mutex<Keyboard>>,
    last_display_length: usize,
}

impl Displayer {
    pub fn new(keyboard: Arc<Mutex<Keyboard>>) -> Self {
        Self {
            keyboard,
            last_display_length: 0,
        }
    }

    pub fn update(&mut self, player: &Player) {
        self.erase(false);

        let normal_command_buffer = lock!(self.keyboard, keyboard, {
            keyboard.buffer().to_string()
        });

        let mut message = String::new();
        if player.is_paused() {
            message.push_str("[paused] ");
        }
        message.push_str(&format!("{} bpm", Self::format_tempo(player.tempo())));
        if player.beats_per_bar() != 0 {
            let beat = player.beat().to_string().color(Color::Blue);
            let beats_per_bar = player.beats_per_bar().to_string().color(Color::Blue);
            message.push_str(&format!(" |{}/{}| ", beat, beats_per_bar));
        }
        if !normal_command_buffer.is_empty() {
            let s = format!("  {}", normal_command_buffer);
            message.push_str(&s.color(Color::Black).bold().to_string());
        }

        self.last_display_length = message.len();
        print!("{}", message);

        Self::flush();
    }

    pub fn erase(&mut self, flush: bool) {
        let erase_last_message = " ".repeat(self.last_display_length);
        print!("\r{}\r", erase_last_message);
        self.last_display_length = 0;

        if flush {
            Self::flush();
        }
    }

    fn format_tempo(tempo: f32) -> String {
        format!("{:.1$}", tempo, TEMPO_PRECISION)
            .trim_end_matches('0')
            .trim_end_matches('.')
            .to_string()
    }

    fn flush() {
        io::stdout().flush().unwrap();
    }
}
