use nom::{IResult, Err};
use nom::bytes::streaming::tag;
use nom::character::streaming::one_of;
use nom::branch::alt;
use nom::combinator::{map, opt};
use nom::sequence::{terminated, pair};

use crate::util::parse_utils::{float, integer};
use crate::commands::{Command, ModifDirection, ModifDetail};

pub const DEFAULT_TEMPO_STEP: f32 = 1.0;

pub struct Keyboard {
    buffer: String,
    last_tempo_modif: ModifDetail,
}

impl Keyboard {
    pub fn new() -> Self {
        Self {
            buffer: String::new(),
            last_tempo_modif: ModifDetail::default(),
        }
    }

    pub fn buffer(&self) -> &str {
        &self.buffer
    }

    pub fn get_command_from_new_char(&mut self, new_char: char) -> Option<Command> {
        match new_char {
            '\x08' | '\x7f' => {
                self.buffer.pop();
            },
            _ => {
                self.buffer.push(new_char);
            },
        }

        match parse_normal_command(&self.buffer) {
            Ok((_remaining, command)) => {
                self.buffer.clear();
                Some(command)
            },
            Err(err) => {
                if !matches!(err, Err::Incomplete(_)) {
                    self.buffer.clear();
                }
                None
            },
        }
    }

    pub fn apply_tempo_modif(&mut self, n: f32, modif: &ModifDetail) -> f32 {
        if let Some(value) = modif.value {
            self.last_tempo_modif.value = Some(value);
        }
        self.last_tempo_modif.direction = modif.direction;

        self.last_tempo_modif.apply(n, DEFAULT_TEMPO_STEP)
    }
}

fn parse_normal_command(input: &str) -> IResult<&str, Command> {
    alt((
        map(tag("q"), |_| Command::Quit),
        map(tag(" "), |_| Command::TogglePause),
        map(
            pair(
                opt(float::<f32>),
                one_of("+-"),
            ),
            |(n, sign)| {
                let direction = match sign {
                    '+' => ModifDirection::Increase,
                    '-' => ModifDirection::Decrease,
                    _ => unreachable!(),
                };
                Command::new_modify_tempo(direction, n)
            },
        ),
        map(
            terminated(float, tag("\n")),
            |n: f32| Command::SetTempo(n)
        ),
        map(
            terminated(integer, tag("*")),
            |n: i16| Command::SetBeatsPerBar(n)
        )
    ))
    (input)
}
