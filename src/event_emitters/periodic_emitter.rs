
pub struct PeriodicEmitter {
    period: f32,
    next_event: f32,
    callbacks: Vec<Box<dyn PeriodicEmiterCallback>>,
}

impl PeriodicEmitter {
    pub fn new(clock: f32, period: f32) -> Self {
        Self {
            period,
            next_event: clock,
            callbacks: vec![],
        }
    }

    pub fn restart(&mut self, clock: f32) {
        self.next_event = clock;
    }

    pub fn set_period(&mut self, period: f32) {
        self.period = period;
    }

    pub fn subscribe(&mut self, callback: impl PeriodicEmiterCallback) {
        self.callbacks.push(Box::new(callback));
    }

    pub fn trigger(&mut self, clock: f32) {
        for callback in &mut self.callbacks {
            callback(clock);
        }
        self.next_event = clock + self.period;
    }

    pub fn next_tick(&mut self, clock: f32) {
        if clock > self.next_event {
            self.trigger(clock);
        }
    }
}

pub trait PeriodicEmiterCallback: FnMut(f32) + Send + 'static {}

impl<F> PeriodicEmiterCallback for F where F: FnMut(f32) + Send + 'static {}
