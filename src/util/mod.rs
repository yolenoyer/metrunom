pub mod parse_utils;
pub mod send_ptr;

pub use send_ptr::SendPtr;

use getch::Getch;

pub fn getchar(getch: &Getch) -> char {
    let mut bytes = vec![];
    let mut n = 4;
    loop {
        let b = getch.getch().unwrap();
        bytes.push(b);
        match String::from_utf8(bytes.clone()) {
            Ok(s) => return s.chars().next().unwrap(),
            Err(_) => (),
        }
        n -= 1;
        if n == 0 {
            panic!("Keyboard error");
        }
    }
}

pub fn period_from_tempo(tempo: f32) -> f32 {
    1.0 / tempo * 60.0
}
