macro_rules! lock {
    ($player:expr,$ident:ident, $block:block) => {{
        #[allow(unused_mut)]
        let mut $ident = $player.lock().unwrap();
        $block
    }};
}
