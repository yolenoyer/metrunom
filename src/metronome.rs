use crate::sound::ClickGenerator;
use crate::event_emitters::PeriodicEmitter;
use crate::util::{period_from_tempo, SendPtr};
use crate::sound::SoundGenerator;

pub struct InnerMetronome {
    click_generator: ClickGenerator,
    beat: i16,
    beats_per_bar: i16,
    frequency: f32,
    need_refresh: bool,
}

pub struct Metronome {
    tempo: f32,
    periodic_emitter: PeriodicEmitter,
    inner_metronome: Box<InnerMetronome>,
}

impl Metronome {
    pub fn new(tempo: f32, beats_per_bar: i16, frequency: f32) -> Self {
        let click_generator = ClickGenerator::new(frequency);

        let inner_metronome = InnerMetronome {
            click_generator,
            beat: -1,
            beats_per_bar,
            frequency,
            need_refresh: false,
        };

        let mut metronome = Self {
            tempo,
            periodic_emitter: PeriodicEmitter::new(0.0, period_from_tempo(tempo)),
            inner_metronome: Box::new(inner_metronome),
        };

        let inner_metronome_ref = SendPtr(metronome.inner_metronome.as_mut());
        metronome.periodic_emitter.subscribe(move |clock: f32| {
            let inner_metronome = unsafe { inner_metronome_ref.0.as_mut().unwrap() };
            Self::trigger_click(inner_metronome, clock);
        });

        metronome
    }

    fn trigger_click(m: &mut InnerMetronome, clock: f32) {
        let mut frequency = m.frequency;
        if m.beats_per_bar != 0 {
            m.beat = (m.beat + 1) % m.beats_per_bar;
            if m.beat % m.beats_per_bar == 0 {
                frequency *= 1.5;
            }
        }

        m.click_generator.set_frequency(frequency);
        m.click_generator.play(clock);
        m.need_refresh = true;
    }

    pub fn need_refresh(&self) -> bool {
        self.inner_metronome.need_refresh
    }

    pub fn reset_need_refresh(&mut self) {
        self.inner_metronome.need_refresh = false;
    }

    pub fn set_tempo(&mut self, tempo: f32) {
        self.tempo = tempo;
        self.periodic_emitter.set_period(period_from_tempo(tempo));
    }

    pub fn beat(&self) -> i16 {
        self.inner_metronome.beat
    }

    pub fn beats_per_bar(&self) -> i16 {
        self.inner_metronome.beats_per_bar
    }

    pub fn set_beats_per_bar(&mut self, beats_per_bar: i16) {
        self.inner_metronome.beats_per_bar = beats_per_bar;
    }

    pub fn restart(&mut self, clock: f32) {
        self.inner_metronome.beat = -1;
        self.periodic_emitter.restart(clock);
    }
}

impl SoundGenerator for Metronome {
    fn get_sample(&mut self, clock: f32) -> f32 {
        self.periodic_emitter.next_tick(clock);
        self.inner_metronome.click_generator.get_sample(clock)
    }
}
