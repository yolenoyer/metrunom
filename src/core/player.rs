use std::sync::{Arc, Mutex};
// use std::fs::File;
// use std::io::Write;

use crate::sound::AntiPopFader;
use crate::metronome::Metronome;
use crate::sound::SoundGenerator;
use crate::interface::Displayer;

// A given amount of silent ticks (amplitude=0) played at the startup of the program, in order to
// avoid unpleasant pops:
const STARTING_TICKS: u32 = 16384;

pub const MAX_TEMPO: f32 = 1000.0;
pub const TEMPO_PRECISION: usize = 2;

pub struct Player {
    // Constant properties:
    sample_rate: u32,
    main_volume: f32,

    // State:
    tick: i64,
    is_paused: bool,
    tempo: f32,
    starting_ticks: u32,

    stopping: bool,
    global_anti_pop_fader: AntiPopFader,

    metronome: Metronome,

    pub displayer: Option<Arc<Mutex<Displayer>>>,

    // output_file: Option<File>,
}

impl Player {
    pub fn new(
        config: &cpal::StreamConfig,
        tempo: f32,
        beats_per_bar: i16,
        main_volume: f32,
        default_frequency: f32
    ) -> Self {
        let mut player = Self {
            sample_rate: config.sample_rate.0,
            main_volume,
            tick: 0,
            is_paused: false,
            tempo,
            starting_ticks: STARTING_TICKS,
            stopping: false,
            global_anti_pop_fader: AntiPopFader::new(),
            metronome: Metronome::new(tempo, beats_per_bar, default_frequency),
            displayer: None,
            // output_file: Some(File::create("values").unwrap()),
        };

        player.global_anti_pop_fader.fade_in(0.0);

        player
    }

    pub fn set_displayer(&mut self, displayer: Arc<Mutex<Displayer>>) {
        self.displayer = Some(displayer);
    }

    pub fn clock(&self) -> f32 {
        self.tick as f32 / self.sample_rate as f32
    }

    pub fn stop(&mut self) {
        self.stopping = true;
    }

    pub fn is_paused(&self) -> bool {
        self.is_paused
    }

    pub fn pause(&mut self) {
        let clock = self.clock();
        self.is_paused = true;
        self.global_anti_pop_fader.fade_out(clock);
    }

    pub fn unpause(&mut self) {
        let clock = self.clock();
        self.is_paused = false;
        self.metronome.restart(clock);
        self.global_anti_pop_fader.fade_in(clock);
    }

    pub fn toggle_pause(&mut self) {
        if self.is_paused {
            self.unpause();
        } else {
            self.pause();
        }
    }

    pub fn restart(&mut self) {
        self.metronome.restart(self.clock());
    }

    pub fn tempo(&self) -> f32 {
        self.tempo
    }

    pub fn set_tempo(&mut self, tempo: f32) {
        if tempo <= 0.0 || tempo > MAX_TEMPO {
            return;
        }
        let pow_of_10 = 10.0_f32.powi(TEMPO_PRECISION as i32);
        self.tempo = (tempo * pow_of_10).round() / pow_of_10;
        self.metronome.set_tempo(self.tempo);
    }

    pub fn beat(&self) -> i16 {
        self.metronome.beat()
    }

    pub fn beats_per_bar(&self) -> i16 {
        self.metronome.beats_per_bar()
    }

    pub fn set_beats_per_bar(&mut self, beats_per_bar: i16) {
        self.metronome.set_beats_per_bar(beats_per_bar);
    }

    pub fn refresh_display(&mut self) {
        if self.metronome.need_refresh() {
            if let Some(displayer) = &self.displayer {
                lock!(displayer, displayer, {
                    displayer.update(&self);
                });
                self.metronome.reset_need_refresh();
            }
        }
    }

    pub fn next_sample(&mut self) -> f32 {
        if self.starting_ticks != 0 {
            self.starting_ticks -= 1;
            return 0.0;
        }

        self.tick += 1;
        let clock = self.clock();

        if self.stopping {
            self.global_anti_pop_fader.fade_out(clock);
            self.stopping = false;
        }

        let sample = self.metronome.get_sample(clock);

        let final_sample = sample * self.global_anti_pop_fader.get_amp(clock) * self.main_volume;

        // if let Some(file) = &mut self.output_file {
        //     let s = format!("{}\n", final_sample);
        //     file.write_all(s.as_bytes()).unwrap();
        // }

        final_sample
    }
}
