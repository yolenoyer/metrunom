
enum State {
    Off,
    Attack(f32),
    Decay(f32),
    Sustain(f32),
    Release(f32),
}

pub struct AdsrEnvelope {
    attack: f32,
    decay: f32,
    sustain: f32,
    release: f32,
    peak_level: f32,
    sustain_level: f32,
    state: State,
}

impl AdsrEnvelope {
    pub fn new(a: f32, d: f32, s: f32, r: f32, pl: f32, sl: f32) -> Self {
        Self {
            attack : a,
            decay  : d,
            sustain: s,
            release: r,
            peak_level: pl,
            sustain_level: sl,
            state: State::Off,
        }
    }

    pub fn start(&mut self, clock: f32) {
        self.state = State::Attack(clock);
    }

    pub fn get_amp(&mut self, clock: f32) -> f32 {
        match self.state {
            State::Off => 0.0,
            State::Attack(start_time)  => {
                let progress = Self::get_progress(start_time, clock, self.attack);
                if progress == 1.0 {
                    self.state = State::Decay(clock);
                }
                self.peak_level * progress
            },
            State::Decay(start_time)   => {
                let progress = Self::get_progress(start_time, clock, self.decay);
                if progress == 1.0 {
                    self.state = State::Sustain(clock);
                }
                self.sustain_level + (self.peak_level - self.sustain_level) * (1.0 - progress)
            },
            State::Sustain(start_time) => {
                let progress = Self::get_progress(start_time, clock, self.sustain);
                if progress == 1.0 {
                    self.state = State::Release(clock);
                }
                self.sustain_level
            },
            State::Release(start_time) => {
                let progress = Self::get_progress(start_time, clock, self.release);
                if progress == 1.0 {
                    self.state = State::Off;
                }
                self.sustain_level * (1.0 - progress)
            },
        }
    }

    fn get_progress(start_time: f32, clock: f32, stage_duration: f32) -> f32 {
        if stage_duration == 0.0 {
            1.0
        } else {
            let progress_duration = clock - start_time;
            (progress_duration / stage_duration).min(1.0)
        }
    }
}
