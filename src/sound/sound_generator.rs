
pub trait SoundGenerator {
    fn get_sample(&mut self, clock: f32) -> f32;
}
