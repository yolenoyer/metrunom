
pub const FADE_DURATION: f32 = 0.005;

enum AntiPopState {
    FadeIn(f32),
    FadeOut(f32),
    On,
    Off,
}

pub struct AntiPopFader {
    state: AntiPopState,
}

impl AntiPopFader {
    pub fn new() -> Self {
        Self {
            state: AntiPopState::Off,
        }
    }

    pub fn fade_in(&mut self, clock: f32) {
        match self.state {
            AntiPopState::FadeOut(start_time) => {
                let pseudo_fadein_start_time = Self::get_pseudo_start_time(clock, start_time);
                self.state = AntiPopState::FadeIn(pseudo_fadein_start_time);
            },
            AntiPopState::Off => {
                self.state = AntiPopState::FadeIn(clock);
            },
            _ => (),
        }
    }

    pub fn fade_out(&mut self, clock: f32) {
        match self.state {
            AntiPopState::FadeIn(start_time) => {
                let pseudo_fadeout_start_time = Self::get_pseudo_start_time(clock, start_time);
                self.state = AntiPopState::FadeOut(pseudo_fadeout_start_time);
            },
            AntiPopState::On => {
                self.state = AntiPopState::FadeOut(clock);
            },
            _ => (),
        }
    }

    pub fn get_amp(&mut self, clock: f32) -> f32 {
        match self.state {
            AntiPopState::FadeIn(start_time) => {
                let amp = (clock - start_time) / FADE_DURATION;
                if amp > 1.0 {
                    self.state = AntiPopState::On;
                    1.0
                } else {
                    amp
                }
            },
            AntiPopState::FadeOut(start_time) => {
                let amp = 1.0 - (clock - start_time) / FADE_DURATION;
                if amp < 0.0 {
                    self.state = AntiPopState::Off;
                    0.0
                } else {
                    amp
                }
            },
            AntiPopState::On => 1.0,
            AntiPopState::Off => 0.0,
        }
    }

    fn get_pseudo_start_time(clock: f32, last_start_time: f32) -> f32 {
        let progress_duration = clock - last_start_time;
        let remaining_fade_duration = FADE_DURATION - progress_duration;
        clock - remaining_fade_duration
    }
}
